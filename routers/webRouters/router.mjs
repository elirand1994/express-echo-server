import express from 'express';
var router = express.Router();
import log from '@ajar/marker';

const functionLogger = (req,res,next) =>{
    log.cyan('Only logs at adding new user..')
    next();
}
router.post('/newUser',functionLogger,(req,res)=>{
    const {user_id,password,email} = req.body 
    const responseObj = {
        user_id : user_id,
        password : password,
        email : email
    }
    res.status(200).send(responseObj)
})

router.get('/search',(req,res)=>{
    const food = req.query.food;
    const town = req.query.town;
    const responseObj = {
        food : food,
        town : town
    }
    res.status(200).send(responseObj);
})

router.get('/search/:id', (req,res)=>{
    const id = req.query.id;
    res.status(200).send(id);
})

export default router;