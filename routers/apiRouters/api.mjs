
import express from 'express';

var router = express.Router();
import log from '@ajar/marker';

const logger = (req,res,next) =>{
    log.cyan('Logging the data to the db...');
    log.yellow('-------------------');
    log.red(`REQ URL : ${req.url}`)
    next();
}

router.use(logger);

router.get('/',  (req, res) => {
    res.status(200).send('Hello Express!')
})

router.get('/users', (req, res) => {
    res.status(200).send('Get all Users')
})



router.get('/markup',(req,res)=>{

    // res.set('Content-Type','text/html');

    let some_data = 'This is a markup response!';
    
    const markup = `<h1>Hello Express</h1>
                    <p>This is an example demonstrating some basic html markup<br/>
                        being sent and rendered in the browser</p>
                    <p>Prepare to be:</p>
                    <ul>
                        <li>Surprised!</li>
                        <li>Amazed!</li>
                        <li>${some_data}</li>  
                    </ul>`
    res.status(200).set('Content-Type', 'text/html').send(markup)
    // res.status(200).send(markup)
})

export default router;