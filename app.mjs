import express from 'express';
import morgan from 'morgan';
import apiRouter from './routers/apiRouters/api.mjs';
import webRouter from './routers/webRouters/router.mjs';

import log from '@ajar/marker';
const appRouter = express.Router();

const { PORT, HOST } = process.env;

const app = express()

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan('dev'));

app.use('/api',apiRouter);
app.use(webRouter);
app.use(appRouter);

appRouter.use('*', (req,res)=>{
    res.status(404).send("URL is broken!")
})

app.listen(PORT, HOST,  ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});

//------------------------------------------
//         Express Echo Server
//------------------------------------------
/* challenge instructions

     - install another middleware - morgan
        configuring app middleware like so:
        app.use( morgan('dev') );

    -  define more routing functions that use

        - req.query - access the querystring part of the request url
        - req.params - access dynamic parts of the url
        - req.body - access the request body of a POST request
        
        in each routing function you want to pass some values to the server from the client
        and echo those back in the server response

    - return api json response
    - return html markup response

    - return 404 status with a custom response to unsupported routes


*/
